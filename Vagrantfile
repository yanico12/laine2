# Début du bloc de configuration global de Vagrant
Vagrant.configure("2") do |config|
  # Configuration générale applicable à toutes les VM
  config.vm.box = "bento/ubuntu-20.04"
  
  # Définition des nœuds dans un array pour une configuration simplifiée
  nodes = [
    { name: "k8s-master", role: "master", script: "master.sh" },
    { name: "worker-1", role: "worker", script: "node.sh" },
    { name: "worker-2", role: "worker", script: "node.sh" }
  ]

  # Configuration des nœuds
  nodes.each do |node|
    config.vm.define node[:name] do |node_config|
      node_config.vm.hostname = node[:name]
      node_config.vm.network "private_network", type: "dhcp"
      config.vm.synced_folder "C:/Users/fabri/Desktop/laine2", "/home/vagrant/laine"

      node_config.vm.provider "vmware_desktop" do |v|
        v.vmx["memsize"] = "2048"
        v.vmx["numvcpus"] = "2"
        v.gui = false
        v.vmx["ethernet0.connectionType"] = "nat"
        v.vmx["virtualHW.version"] = "11"
        v.vmx["ethernet0.present"] = "true"
        v.vmx["scsi0:0.size"] = "50000"
      end

      # Configuration de provisionnement Ansible pour tous les nœuds
      node_config.vm.provision "ansible_local" do |ansible|
        ansible.playbook = "playbook.yml"
        ansible.extra_vars = {
          node_role: node[:role]
        }
      end

      # Configuration de provisionnement shell spécifique à chaque nœud
      node_config.vm.provision "shell", path: node[:script]
    end
  end
end # Fin du bloc de configuration global de Vagrant
