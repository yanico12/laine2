#!/bin/bash

# S'assurer que le système est à jour
sudo apt update && sudo apt upgrade -y
sudo apt install python3 -y

# Installer les dépendances requises pour Ansible
sudo apt install -y software-properties-common

# Ajouter le dépôt officiel d'Ansible et installer Ansible
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" | sudo tee -a /etc/apt/sources.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt update && sudo apt install -y ansible

# Vérifier l'installation
ansible --version
