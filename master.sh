#!/bin/bash

sudo apt install ssh
# Mettre à jour les paquets
sudo apt-get update && sudo apt-get upgrade -y

# Installer les dépendances nécessaires
sudo apt-get install -y apt-transport-https ca-certificates curl

# Ajouter la clé GPG du dépôt officiel Kubernetes
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

# Ajouter le dépôt Kubernetes
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Mettre à jour l'index des paquets et installer kubeadm, kubelet et kubectl
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gpg
sudo mkdir -p -m 755 /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg -y
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
sudo systemctl enable --now kubelet
#installer containerd
sudo apt update
sudo apt install -y containerd
#configurer containerd
sudo mkdir -p /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml
#Redémarrer et activer containerd :
sudo mkdir -p /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml
# conservez les parametres apres redemarrage 
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-ip6tables=1" | sudo tee -a /etc/sysctl.conf
#appliquer les modification
sudo sysctl -p


# Désactiver le swap - nécessaire avant d'initialiser Kubernetes
sudo swapoff -a
sudo sed -i '/ swap / s/^/#/' /etc/fstab

# Initialiser le cluster Kubernetes (Remplacer <pod-network-cidr> par le CIDR de votre choix)
#kubeadm non fonctionnel apres l'installation ?
sudo kubeadm reset -f
sudo swapoff -a
sudo kubeadm init --pod-network-cidr=192.168.0.0/16

# Pour configurer kubectl pour l'utilisateur vagrant
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Installer le réseau de pods Flannel
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# script pour l'installation de docker
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo usermod -aG docker $USER
sudo docker version
echo "Installation de Docker terminée avec succès."
echo "Vous devrez vous déconnecter et vous reconnecter pour que les modifications prennent effet."
sudo dpkg-reconfigure docker
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io







# Créer un ServiceAccount
kubectl create serviceaccount my-serviceaccount -n kube-system

# Attribuer le rôle cluster-admin au ServiceAccount
kubectl create clusterrolebinding my-serviceaccount-rolebinding --clusterrole=cluster-admin --serviceaccount=kube-system:my-serviceaccount

# Obtenir le token du ServiceAccount
SECRET_NAME=$(kubectl get serviceaccount my-serviceaccount -n kube-system -o jsonpath="{.secrets[0].name}")
kubectl get secret $SECRET_NAME -n kube-system -o jsonpath="{.data.token}" | base64 --decode
#si erreur d'affichage du kubernetes, faire se code pour debugger
kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous
