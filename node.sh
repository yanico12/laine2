#!/bin/bash

# Installer les mêmes paquets que pour le master
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install -y apt-transport-https ca-certificates curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

# Désactiver le swap - nécessaire pour Kubernetes
sudo swapoff -a
sudo sed -i '/ swap / s/^/#/' /etc/fstab

# À ce point, vous devrez rejoindre le cluster manuellement en utilisant
# la commande fournie par `kubeadm init` exécuté sur le master.
# Exemple: sudo kubeadm join --token <token> <master-ip>:6443 --discovery-token-ca-cert-hash sha256:<hash>
